﻿using System;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

public class HelloWorld
{
    public static void Main()
    {
        Document doc = new Document();
        PdfWriter.GetInstance(doc,
            new FileStream("itext.pdf", FileMode.Create)
        );
        doc.Open();
        Paragraph p = new Paragraph("Look! My First PDF!");
        doc.Add(p);
        doc.Close();
        Console.WriteLine("Created a PDF!");
    }
}
